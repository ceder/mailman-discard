#!/usr/bin/env python

# If this is run as a console application in a fresh window, I want
# the window to stick around when the process exits.  As a first
# approximation this delayed exit happens when running under win32,
# but this is only an approximation -- a win32 user might run this
# program in a command prompt, and a KDE user might run this in a
# window.

import sys
delay_exit = sys.platform == 'win32'

if delay_exit:
    import traceback

import mailman_discard

try:
    try:
        ret = mailman_discard.main()
    except:
        if delay_exit:
            traceback.print_exc()
        ret = 1
finally:
    if delay_exit:
        raw_input("Press Enter to exit.")
sys.exit(ret)
