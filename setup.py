#!/usr/bin/env python

import os

from distutils.core import setup
from distutils import log
import distutils.command.install_scripts
import distutils.command.bdist_wininst

classifiers="""\
Development Status :: 4 - Beta
Environment :: Console
Environment :: Web Environment
Intended Audience :: End Users/Desktop
Intended Audience :: Information Technology
Intended Audience :: Other Audience
Intended Audience :: System Administrators
License :: OSI Approved :: GNU General Public License (GPL)
Natural Language :: English
Operating System :: OS Independent
Programming Language :: Python
Topic :: Communications :: Email :: Filters
Topic :: Communications :: Email :: Mailing List Servers
Topic :: Internet :: WWW/HTTP :: Browsers
Topic :: Software Development :: Libraries :: Python Modules
Topic :: Utilities
"""

descr = """\
This is a console application, which fetches the list of pending
administrative requests from one or more Mailman lists that you
moderate.  It will then present the messages to you, in batches of 15
(configurable), and if they are all spam you can discard all of them
simply by pressing "y RET".

The mechanize module is used, so you must install it first.
See http://wwwsearch.sourceforge.net/mechanize/

This also includes the choices.py module, which is an implementation
of the Choices system for selecting a configuration file.
See http://rox.sourceforge.net/choices.html
"""

disable_rename = False

class install_scripts(distutils.command.install_scripts.install_scripts):
    def run(self):
        distutils.command.install_scripts.install_scripts.run(self)
        if os.name == 'posix' and not disable_rename:
            for outfile in self.get_outputs():
                root, ext = os.path.splitext(outfile)
                if ext == '.py':
                    log.info("Renaming %s to %s", outfile, root)
                    if not self.dry_run:
                        os.rename(outfile, root)

class bdist_wininst(distutils.command.bdist_wininst.bdist_wininst):
    def run(self):
        global disable_rename
        disable_rename = True
        distutils.command.bdist_wininst.bdist_wininst.run(self)


name="mailman-discard"
version="0.1.1"
setup(name=name,
      version=version,
      description="Interactively discard pending mail from Mailman queues.",
      author="Per Cederqvist",
      author_email="ceder@lysator.liu.se",
      url="http://www.lysator.liu.se/~ceder/%s/" % name,
      download_url=("ftp://ftp.lysator.liu.se"
                    "/pub/mail/%s/%s-%s.tar.gz" % (name, name, version)),

      packages=['mailman_discard'],
      scripts=['mailman-discard.py'],

      cmdclass={'install_scripts': install_scripts,
                'bdist_wininst': bdist_wininst,
                },
      
      license="GNU GPL",
      long_description=descr,
      classifiers = filter(None, classifiers.split("\n")),
      platforms = [""],
      keywords = "mailman discard moderate mail email mailinglist"
     )
