#! /usr/bin/env python
# mailman_discard -- interactively discard spam from mailman lists
# Copyright (C) 2004 Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import re
import sys
import email
import email.Header

import mechanize
import ClientForm

import choices

DEFAULT_BATCH = 15

class Message(object):
    def __init__(self, listname, name, browser, form):
        self.__listname = listname
        self.__browser = browser
        self.__name = name
        self.__form = form
        # self.__action = form[name]
        # self.__preserve = form["preserve-" + name]
        # self.__forward = form["forward-" + name]
        # self.__forward_addr = form["forward-addr-" + name]
        # self.__comment = form["comment-" + name]
        self.__headers = form["headers-" + name]
        # self.__fulltext = form["fulltext-" + name]

        self.__headers = email.message_from_string(self.__headers)
        self.__subject = self.header('subject')
        self.__sortkey = self.__subject + str(id(self))

    def sortkey(self):
        return self.__sortkey

    def header(self, hdr):
        s = self.__headers[hdr]
        decoded = email.Header.decode_header(s)
        try:
            h = email.Header.make_header(decoded)
            res = unicode(h)
            return res.encode('utf-8')
        except (LookupError, UnicodeDecodeError):
            return s

    def subject(self):
        return self.__subject

    def listname(self):
        return self.__listname

    def spam_status(self):
        s = self.header('x-spam-status')
        m = re.search('(Yes|No), hits=([0-9.]+) ', s)
        if m is None:
            self.__spam_score = None
            return '?' * 10
        else:
            if m.group(1) == 'No':
                c = '-'
            else:
                c = '*'
            self.__spam_score = float(m.group(2))
            score = int(self.__spam_score)
            if score > 10:
                s = ' %s ' % m.group(2)
                s = c * ((10-len(s)) / 2) + s
                s = s + c * (10-len(s))
                return s
            else:
                return c * score

    def discard(self):
        self.__browser[self.__name] = ["3"]
        self.__browser.md_need_submit = self.__listname

def find_messages(url, listname, password):
    b = mechanize.Browser()
    b.open(url + "/" + listname)

    b.select_form(nr=0)
    b['adminpw'] = password
    b.submit('request_login')
    if len(b.forms()) == 0:
        return None, []
    b.select_form(nr=0)

    # Find the control names.
    form = b.forms()[0]
    i = 0
    names = []
    while True:
        try:
            names.append(form.find_control(type="radio",nr=i).name)
        except ClientForm.ControlNotFoundError:
            break
        i += 1

    return b, [Message(listname, name, b, form) for name in names]

def fetch(lists):
    all_msgs = []
    browsers = []
    for url, listname, password in lists:
        print "Fetching", listname + "..."
        b, msgs = find_messages(url, listname, password)
        if b is not None:
            browsers.append(b)
        all_msgs.extend(msgs)
    print

    sort_list = [(m.sortkey(), m) for m in all_msgs]
    sort_list.sort()
    all_msgs = [m for k, m in sort_list]
    return browsers, all_msgs

def doit(lists, batch_size):
    browsers, msgs = fetch(lists)

    for b in browsers:
        b.md_need_submit = None

    query_user(msgs, batch_size)

    for b in browsers:
        if b.md_need_submit is not None:
            print "Submitting %s..." % b.md_need_submit
            b.submit()

def query_user(msgs, batch_size):
    FMT = "%-22s %-10.10s %.44s"
    for batch in range(0, len(msgs), batch_size):
        while True:
            print FMT % ("List", "Spam", "Subjects")
            print FMT % ("="*15, "="*10, "="*50)
            for i in range(batch, min(batch + batch_size, len(msgs))):
                print FMT % (msgs[i].listname(),
                             msgs[i].spam_status(),
                             msgs[i].subject())

            response = raw_input("\nDiscard all the above? (y/n/c) ")
            if response == "n":
                break
            if response == "y":
                for i in range(batch, min(batch + batch_size, len(msgs))):
                    msgs[i].discard()
                break
            if response == "c":
                return


def main():
    lists = read_cfg_lists()
    if lists is None:
        return 1
    doit(lists, read_cfg_batch_size())
    return 0

def read_cfg_lists():
    fn, not_here = choices.find_first_return_candidates('mailman-discard',
                                                        'lists')
    if fn is None:
        sys.stderr.write(
            "Lists configuration file not found in any of these locations:\n")
        for fn in not_here:
            sys.stderr.write("  %s\n" % fn)
        sys.stderr.write("You can set CHOICESPATH to change the list.\n")
        return None

    fp = open(fn)
    sites = []
    for line in fp.readlines():
        if line and line[0] != "#":
            url, listname, password = line.split(None, 2)
            sites.append((url, listname, password))
    fp.close()
    return sites

def read_cfg_batch_size():
    fn = choices.find_first('mailman-discard', 'batch')
    if fn == None:
        return DEFAULT_BATCH
    fp = open(fn)
    res = None
    for line in fp.readlines():
        if line and line[0] != "#":
            if res is not None:
                sys.stderr.write("Trailing garbage in %s ignored.\n" % fn)
                break
            res = int(line)
    fp.close()
    if res is None:
        sys.stderr.write("No batch size found in %s.\n" % fn)
        res = DEFAULT_BATCH
    return res

if __name__ == '__main__':
    main()
