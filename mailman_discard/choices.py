# choices.py -- implement the Choices system for finding config files.
# Copyright (C) 2004 Per Cederqvist <ceder@lysator.liu.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


"""Implement the Choices system for finding configuration files.

The Choices system is defined on http://rox.sourceforge.net/choices.html.
"""

import os
import errno

__all__ = ['find_first', 'find_all', 'find_save', 'open_save',
           'want_save', 'save_cfg']

_path = None

def _setup_path():
    """Set _path based on CHOICESPATH or the default list."""

    global _path

    if _path is None:
        path = os.getenv('CHOICESPATH')
        if path is None:
            _path = []
            home = os.getenv('HOME')
            if home is not None:
                _path.append(os.path.join(home, 'Choices'))
            _path.append('/usr/local/share/Choices')
            _path.append('/usr/share/Choices')
        else:
            _path = path.split(os.pathsep)

def find_first(program, file):
    """Return the first existing configuration file.

     - program -- the name of the program.
     - file -- the name of the configuration file.

    Search $CHOICESPATH (or the default list) until an existing file
    is found, and return the file name.  Return None if no file was
    found.
    """
    return find_first_return_candidates(program, file)[0]

def find_first_return_candidates(program, file):
    """Like find_first, but also return a list of all candidates.

    The return value is a 2-tuple of the found file name (or None),
    and all the candidate names that didn't exist.  The latter list
    can be useful if you want to tell the user where you looked for
    the configuration.
    """
    _setup_path()
    notfound = []
    for path in _path:
        if path != '':
            filename = os.path.join(path, program, file)
            if os.path.exists(filename):
                return filename, notfound
            else:
                notfound.append(filename)
    return None, notfound

def find_all(program, file):
    """Return all existing configuration files.
    
     - program -- the name of the program.
     - file -- the name of the configuration file.

    Search $CHOICESPATH (or the default list) and return a list of all
    file names that exist, in the order they are found.  Return an
    empty list if no file was found.
    """

    _setup_path()
    res = []
    for path in _path:
        if path != '':
            filename = os.path.join(path, program, file)
            if os.path.exists(filename):
                res.append(filename)
    return res

def find_save(program, file):
    """Return the file name for saving configurations.

     - program -- the name of the program.
     - file -- the name of the configuration file.

    Examine $CHOICESPATH (or the default list) and return the file
    name where configuration should be saved, or None if the user
    requested that no automatic configuration changes should be made.
    """

    _setup_path()
    if _path[0] == '':
        return None
    return os.path.join(_path[0], program,file)

def open_save(program, file):
    """Open a file for saving configuration.

     - program -- the name of the program.
     - file -- the name of the configuration file.

    Examine $CHOICESPATH (or the default list) and open a file for
    saving the configuration.

    This returns a tuple of two items:
     - filename
     - temporary filename
     - file descriptor
    If the user has requested that no configuration should be saved,
    all three values will be None.

    This function actually opens a temporary filename (currently by
    appending '.tmp' to the real filename).  You must rename it once
    you are done writing to it.  Sample use:

        fn, fn_tmp, fp = choices.open_save('foo', 'bar')
        if fp is not None:
            fp.write('some config')
            fp.close()
            os.rename(fn_tmp, fn)
    """

    filename = find_save(program, file)
    tmp_filename = filename + '.tmp'
    if filename is None:
        return None, None, None

    try:
        fp = open(tmp_filename, 'w')
    except IOError, err:
        if err.errno == errno.ENOENT:
            os.makedirs(os.path.dirname(tmp_filename))
            fp = open(tmp_filename, 'w')
        else:
            raise

    return filename, tmp_filename, fp

def want_save():
    """Return True if the user wants configuration to be saved."""
    _setup_path()
    return _path[0] != ''

def save_cfg(program, file, cfg):
    """Save a configuration file.

     - program -- the name of the program.
     - file -- the name of the configuration file.
     - cfg -- the new contents of the configuration file.

    Sample use:

        if choices.want_save():
            cfg = generate_new_configuration()
            choices.save_cfg('foo', 'bar', cfg)

    If the user doesn't want to save configuration, this function
    doesn't do anything.  So, if the generate_new_configuration() is a
    cheap function, the above could be implemented as:

        choices.save_cfg('foo', 'bar', generate_new_configuration())

    """
    
    fn, fn_tmp, fp = open_save(program, file)
    if fp is not None:
        fp.write(cfg)
        fp.close()
        os.rename(fn_tmp, fn)
